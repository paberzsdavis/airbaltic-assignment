const state = () => ({
    categories: [
        {
            id: 1,
            name: 'Cabin crew',
            slug: 'cabin-crew'
        },
        {
            id: 2,
            name: 'Pilots',
            slug: 'pilots'
        },
        {
            id: 3,
            name: 'Technicians',
            slug: 'technicians'
        },
        {
            id: 4,
            name: 'On the ground',
            slug: 'on-the-ground'
        },
        {
            id: 5,
            name: 'Trainees',
            slug: 'trainees'
        }
    ],
    jobs: [
        {
            id: 1,
            title: 'Pilot Recruitment Manager',
            slug: 'pilot-recruitment-manager',
            category_slug: 'on-the-ground',
            category_id: 4,
            department: 'HR',
            description: 'Pilot Recruitment Manager airBaltic, Europe’s favourite airline have an exciting opportunity for an experienced aviation recruiter to come onboard and manage our Direct Entry Pilot Recruitment function.'
        },
        {
            id: 2,
            title: 'Junior Aircraft Engineer - Europe (Multiple Locations)',
            slug: 'junior-aircraft-engineer-europe',
            category_slug: 'trainees',
            category_id: 5,
            department: 'Engineering Training',
            description: 'We are currently recruiting for the Junior Aircraft Engineer Program (JAE)You will be working in cooperation with our client airline Ryanair, Europe number one low fares airline.'
        },
        {
            id: 3,
            title: 'Head of Digital Marketing',
            slug: 'head-of-digital-marketing',
            category_slug: 'on-the-ground',
            category_id: 4,
            department: 'Sales & Marketing',
            description: 'Head of Digital Marketing Reporting directly to the Chief Marketing Officer, the successful candidate will be based in our Head Office in Airside Business Park in Swords, Dublin.'
        }
    ]
})

export default { state }
