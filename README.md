# airbaltic-assignment

> airbaltic technical task

For project base i'm using [Nuxt.js](https://nuxtjs.org). 
Because bootstrap wasn't an option in this task, i tried not using any other libraries or frameworks.
This week is very busy for me so i haven't finished tabs component. If it's necessary, i can do it next week.
All code related to the task you can find in `assets/sass, components, layouts, pages, plugins, store`.

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
