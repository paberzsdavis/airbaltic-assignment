
export default {
    mode: 'spa',
    /*
    ** Headers of the page
    */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },
    /*
    ** Global CSS
    */
    css: ['~/assets/sass/app.scss'],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: ['~/plugins/Collapse.js',],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: ['@nuxtjs/router'],

    generate: {
        routes: [
            '/'
        ]
    },
    /*
    ** Nuxt.js modules
    */
    modules: [],
    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend (config, ctx) {
            config.module.rules.push({
                test: /\.vue$/,
                loader: "vue-svg-inline-loader",
                options: {}
            })
        }
    }
}
