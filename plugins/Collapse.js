import Vue from 'vue'

function transitionEnd(element, target, callback) {
    target.addEventListener('transitionend', () => {
        let collapseOpen = JSON.parse(element.getAttribute('aria-expanded'))

        target.classList.add('collapse')
        target.removeAttribute('style')
        target.classList.remove('collapsing')

        callback()

        if (collapseOpen) {
            target.classList.add('show')
        }
    })
}

function toggleCollapse(collapseOpen, element, target, close) {
    collapseOpen = close || JSON.parse(element.getAttribute('aria-expanded'))
    element.setAttribute('aria-expanded', !collapseOpen)
    target.classList.remove('collapse')

    if (collapseOpen) {
        target.style.height = `${target.scrollHeight}px`
        target.classList.add('collapsing')
        target.classList.remove('show')

        setTimeout(() => {
            target.style.height = '0'
        }, 50)
        return
    }

    target.classList.add('collapsing')
    setTimeout(() => {
        target.style.height = `${target.scrollHeight}px`
    }, 50)
}

Vue.directive('collapse', {
    bind(element, binding, vnode) {
        let collapseOpen, target, targetEvent = false

        vnode.context.$on('collapse-close', () => {
            if (element.getAttribute('aria-expanded')) {
                toggleCollapse(collapseOpen, element, target, true)
            }
        })

        element.addEventListener('click', () => {
            target = document.querySelector(binding.value)

            if (target.classList.contains('collapsing')) {
                return
            }

            toggleCollapse(collapseOpen, element, target)

            if (!targetEvent) {
                transitionEnd(element, target, () => {
                    targetEvent = true
                })
            }
        })
    }
})
