import Vue from 'vue'
import Router from 'vue-router'

import Jobs from '~/pages/jobs.vue'
import About from '~/pages/about.vue'
import Culture from '~/pages/culture.vue'
import JobsGeneral from '~/pages/jobs-general.vue'
import JobsCategory from '~/pages/jobs-category.vue'
import JobsOpen from '~/pages/jobs-open.vue'

Vue.use(Router)

export function createRouter() {
    return new Router({
        mode: 'history',

        routes: [
            {
                path: '/',
                name: 'about',
                component: About
            },
            {
                path: '/jobs',
                component: Jobs,
                children: [
                    {
                        path: 'all-vacancies',
                        name: 'jobs.general',
                        component: JobsGeneral
                    },
                    {
                        path: ':category/:slug',
                        name: 'jobs.open',
                        component: JobsOpen
                    },
                    {
                        path: ':category',
                        name: 'jobs.category',
                        component: JobsCategory
                    }
                ]
            },
            {
                path: '/culture',
                name: 'culture',
                component: Culture
            }
        ]
    })
}
